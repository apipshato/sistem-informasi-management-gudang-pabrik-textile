<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Deskripsi
<ul>
  <li>Pembuat : <a href="https://gitlab.com/zyfor90?nav_source=navbar">Febriansyah</a></li>
  <li>Nama Aplikasi : Sistem Informasi Management Gudang Pabrik Textile</li>
  <li>Dibuat pada : 7-7-2019</li>
</ul>

## Fitur
<ul>
  <li>Aplikasi Management Users
    <ol>
        <li>Menambahkan Peran/Roles</li>
        <li>Menambahkan Izin/Permissions</li>
        <li>Menambahkan Pengguna/Users</li>
    </ol>
  </li>
  <li>Laporan
    <ol>
      <li>Laporan Harian dengan Print to pdf/excel</li>
      <li>Laporan Mingguan dengan Print to pdf/excel</li>
      <li>Laporan Bulanan dengan Print to pdf/excel</li>
    </ol>
  </li>
</ul>

## Package yang Digunakan
<ul>
  <li>Spatie/Laravel</li>
  <li>Yajra/Laravel</li>
  <li>LaravelCollective/Laravel</li>
</ul>

## Bagaimana Cara install
1. Clone Repository atau Download Manual.
2. Buka Command Line dan masuk ke folder directori.
3. Kemudian Ketik "Composer Install".
4. Copy file .env.example menjadi file .env jika memakai window gunakan perintah ini "Copy .env.example .env".
5. ketikan "php artisan key:generate".
6. Setting database.
7. Migrate table dengan "php artisan migrate".


Terimakasih.
