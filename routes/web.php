<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function(){
  Route::get('/home', 'HomeController@index')->name('home');
  //Route Dashboard
  Route::get('/dashboard', 'DashboardController@index');
  Route::group(['middleware' => ['role:Admin', 'permission:Otorisasi Admin & Akses ke Halaman Admin']], function(){
      //Route Roles
      Route::get('/roles', 'RoleController@index');
      Route::get('/roles/create', 'RoleController@create');
      Route::post('/roles', 'RoleController@store');
      Route::get('/roles/{id}/edit', 'RoleController@edit');
      Route::put('/roles/{id}/update', 'RoleController@update');
      Route::delete('/roles/{id}/delete', 'RoleController@destroy');
      //Route Permissions
      Route::get('/permissions', 'PermissionController@index');
      Route::get('/permissions/create', 'PermissionController@create');
      Route::post('/permissions', 'PermissionController@store');
      Route::get('/permissions/{id}/edit', 'PermissionController@edit');
      Route::put('/permissions/{id}/update', 'PermissionController@update');
      Route::delete('/permissions/{id}/delete', 'PermissionController@destroy');
      //Route Users
      Route::get('/users', 'UserController@index');
      Route::get('/users/create', 'UserController@create');
      Route::post('/users', 'UserController@store');
      Route::get('/users/{id}/edit', 'UserController@edit');
      Route::put('/users/{id}/update', 'UserController@update');
      Route::delete('/users/{id}/delete', 'UserController@destroy');
  });

});
