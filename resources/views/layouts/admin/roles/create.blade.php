@extends('layouts.master')
​
@section('title')
    Pengelolaan Peran
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/permissions') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button></a>
    <a href="{{ url('/users') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Pengguna</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Tambah Peran</h3>
  								</div>
  								<div class="panel-body">
                    {!! Form::open(array('url' => '/roles')) !!}
                      {{ csrf_field() }}
                      <div class="form-group">
                        {!! Form::label("Peran") !!}
                        {!! Form::text("roles", '', array('class' => 'form-control', 'placeholder' => 'Ketikan peran disini')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label('', "Izinkan Untuk Mengakses :") !!} <br>
                        @foreach ($permissions as $permission)
                        {!! Form::checkbox("permissions[]", $permission->id) !!}
                        {{ $permission->name }} <br>
                        @endforeach
                      </div>
                      {!! Form::submit("Tambah", array('class' => 'btn btn-success btn-block')) !!}
                    {!! Form::close() !!}
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
