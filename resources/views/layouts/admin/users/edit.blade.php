@extends('layouts.master')
​
@section('title')
    Pengelolaan Peran
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/roles') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Peran</button></a>
    <a href="{{ url('/permissions') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Edit User</h3>
  								</div>
  								<div class="panel-body">
                    {!! Form::model($user, array('url' => array('/users/'.$user->id.'/update'), 'method' => 'PUT')) !!}
                    {{-- <form class="" action="{{ url('/users/'.$user->id.'/update') }}" method="post"> --}}
                    {{-- <input type="hidden" name="_method" value="PUT"> --}}
                      {{ csrf_field() }}
                      <div class="form-group">
                        {!! Form::label('Nama') !!}
                        {!! Form::text("name", $user->name, array('class' => 'form-control', 'placeholder' => 'isi nama anda disini', 'required')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label('Email') !!}
                        {!! Form::email("email", $user->email, array("class" => 'form-control', 'placeholder' => 'isi email anda disini', 'required')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label("Peran Untuk Users yang akan dibuat :") !!} <br>
                        @foreach ($roles as $role)
                        {!! Form::checkbox("roles[]", $role->id, $user->roles) !!}
                        {!! $role->name !!} <br>
                        @endforeach
                      </div>
                      {{-- <div class="form-group">
                        <label for="">Masukan Password</label>
                        <input type="Password" name="password" class="form-control" value="" placeholder="Masukkan password untuk mengedit" required>
                      </div> --}}
                      {!! Form::submit("Update", array('class' => 'btn btn-success btn-block')) !!}
                      {!! Form::close() !!}
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
