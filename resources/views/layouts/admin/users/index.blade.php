@extends('layouts.master')
​
@section('title')
    Pengelolaan Izin
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <a href="{{ url('/roles') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Peran</button></a>
    <a href="{{ url('/permissions') }}"><button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button></a>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Panel Pengguna</h3>
  								</div>
  								<div class="panel-body">
  									<table class="table table-hover">
  										<thead>
  											<tr>
  												<th>#</th>
  												<th>Nama Pengguna</th>
                          <th>Email</th>
                          <th>Peran</th>
                          <th>Aksi</th>
  											</tr>
  										</thead>
  										<tbody>
  											@for ($i=1; $i <= $users->count(); $i++)
                          @foreach ($users as $user)
                            <tr>
                              <td>{{ $i++ }}</td>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->roles()->pluck('name')->implode(' ') }}</td>
                              <td>
                                {!! Form::model($user, array('url' => array('/users/'.$user->id.'/delete'), 'method' => 'DELETE')) !!}
                                  {{ csrf_field() }}
                                  <a href="{{ url('/users/'.$user->id.'/edit') }}"><button type="button" class="btn btn-info" name="button">Edit</button></a>
                                  {!! Form::submit("Hapus", array('class' => 'btn btn-danger')) !!}
                                {!! Form::close() !!}
                              </td>
                            </tr>
                          @endforeach
                        @endfor
  										</tbody>
  									</table>
                    <a href="{{ url('/users/create') }}"><button type="button" class="btn btn-success" name="button">Tambah Pengguna</button></a>
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
