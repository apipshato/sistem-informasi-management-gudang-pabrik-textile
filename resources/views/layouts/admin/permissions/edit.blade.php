@extends('layouts.master')
​
@section('title')
    Pengelolaan Peran
@endsection
​
@section('content')
  <div class="col-md-3 col-md-offset-9 text-right" style="margin-bottom:15px;">
    <button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Izin</button>
    <button type="button" class="btn btn-default" style="margin:-2.5px;" name="button">Pengguna</button>
  </div>
  <div class="col-md-12">
  							<!-- TABLE HOVER -->
  							<div class="panel">
  								<div class="panel-heading">
  									<h3 class="panel-title">Edit Izin</h3>
  								</div>
  								<div class="panel-body">
                    {!! Form::model($permission, array('url' => array('/permissions/'.$permission->id.'/update'), 'method' => 'PUT')) !!}
                      {{ csrf_field() }}
                      <div class="form-group">
                        {!! Form::label("Izin") !!}
                        {!! Form::text("name", $permission->name, array('class' => 'form-control', 'placeholder' => 'tambahkan izin disini', 'required')) !!}
                      </div>
                      {!! Form::submit("Update", array('class' => 'btn btn-success btn-block')) !!}
                    {!! Form::close() !!}
  								</div>
  							</div>
  							<!-- END TABLE HOVER -->
  						</div>
@endsection
